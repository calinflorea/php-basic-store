<?php

class ProductController extends BaseController
{
    public function addProduct()
    {
        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];
        if (strtoupper($requestMethod) == 'POST') {
            try {
                if (isset($_POST['productType']) && $_POST['productType']) {
                    $produs = new $_POST['productType']();
                    foreach ($_POST as $key => $item) {
                        if ($key != 'productType') {
                            $methodName = 'set' . $key;
                            call_user_func_array(array($produs, $methodName), array($item));//$produs->setSKU($_POST['SKU']);
                        }
                    }
                    $produs->save();
                }
            } catch (Error $e) {
                $strErrorDesc = $e->getMessage() . 'Something went wrong! Please contact support.';
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $strErrorHeader = 'HTTP/1.1 422 Unprocessable Entity';
        }

        if (!$strErrorDesc) {
            $this->sendOutput(
                '',
                array('Location: /', 'HTTP/1.1 201 OK')
            );
        } else {
            $this->sendOutput(
                json_encode(array('error' => $strErrorDesc)),
                array('Content-Type: application/json', $strErrorHeader)
            );
        }
    }

    public function getProducts()
    {
        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];
        if (strtoupper($requestMethod) == 'GET') {
            try {
                $allProducts = array();
                foreach ($this->getTypes() as $type) {
                    $product = new $type;
                    $allProducts = array_merge($allProducts, $product->getProducts());
                }
                usort($allProducts, fn($a, $b) => ($a->getIdProduct() < $b->getIdProduct()) ? -1 : 1);
                $response = json_encode($allProducts);
            } catch (Error $e) {
                $strErrorDesc = $e->getMessage() . 'Something went wrong!';
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $strErrorHeader = 'HTTP/1.1 422 Unprocessable Entity';
        }

        if (!$strErrorDesc) {
            $this->sendOutput(
                $response,
                array('Content-Type: application/json', 'HTTP/1.1 200 OK')
            );
        } else {
            $this->sendOutput(
                json_encode(array('error' => $strErrorDesc)),
                array('Content-Type: application/json', $strErrorHeader)
            );
        }
    }

    private function getTypes()
    {
//        $cmd = 'grep --no-filename -E "^\S*class +.+( *{)?$" ' . "Model" . '/* | cut -d" " -f2';
//        exec($cmd, $output);
//        return $output;
        $types = array();
        foreach (glob('Model/*.php') as $file)
        {
            $class = basename($file, '.php');
            if($class!=="Product") {
                $types[] = $class;
            }
        }
        return $types;
    }

    public function deleteProducts()
    {
        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];
        if (strtoupper($requestMethod) == 'POST') {
            try {
                if (isset($_POST['postSKU']) && $_POST['postSKU']) {
                    $postSKU = $_POST['postSKU'];
                    Product::deleteProducts($postSKU);
                    $response = json_encode("Successfully deleted");
                }
            } catch (Error $e) {
                $strErrorDesc = $e->getMessage() . 'Something went wrong!';
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $strErrorHeader = 'HTTP/1.1 422 Unprocessable Entity';
        }

        if (!$strErrorDesc) {
            $this->sendOutput(
                $response,
                array('Content-Type: application/json', 'HTTP/1.1 200 OK')
            );
        } else {
            $this->sendOutput(
                json_encode(array('error' => $strErrorDesc)),
                array('Content-Type: application/json', $strErrorHeader)
            );
        }
    }
}
