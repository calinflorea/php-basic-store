<?php

class Database
{
    private $connection = null;

    public function __construct()
    {
        try {
            $this->connection = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_DATABASE_NAME, DB_USERNAME, DB_PASSWORD);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }
    }

    public function executeStatement($query = "")
    {
        try {
            $stmt = $this->connection->prepare($query);
            $stmt->execute();
            $this->connection = null;
            return $stmt;
        } catch (PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }
    }

}