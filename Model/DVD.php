<?php

class DVD extends Product implements JsonSerializable
{
    private $size;

    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return [
            'SKU' => $this->SKU,
            'name' => $this->name,
            'price' => $this->price . "$",
            'specificAttribute' => "Size: " . $this->size . "MB"
        ];
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    public function save()
    {
        $sql = "INSERT INTO product(SKU,name,price) VALUES ('$this->SKU','$this->name','$this->price');
                    INSERT INTO dvd(SKU,size) VALUES ('$this->SKU','$this->size')";
        $db = new Database();
        $stmt = $db->executeStatement($sql);
    }

    public function getProducts()
    {
        $sql = "SELECT product.idproduct,product.SKU,product.name,product.price,dvd.size FROM product 
RIGHT JOIN dvd ON product.SKU = dvd.SKU";
        $db = new Database();
        $stmt = $db->executeStatement($sql);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $res = $stmt->fetchAll();
        $products = array();
        foreach ($res as $product) {
            $prod = new DVD();
            $prod->setIdProduct($product["idproduct"]);
            $prod->setSKU($product["SKU"]);
            $prod->setName($product["name"]);
            $prod->setPrice($product["price"]);
            $prod->setSize($product["size"]);
            $products[] = $prod;
        }
        return $products;
    }
}
