<?php

class Furniture extends Product implements JsonSerializable
{
    private $height;
    private $width;
    private $length;

    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return [
            'SKU' => $this->SKU,
            'name' => $this->name,
            'price' => $this->price . "$",
            'specificAttribute' => "Dimensions: " . $this->height . "x" . $this->width . "x" . $this->length
        ];
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height): void
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width): void
    {
        $this->width = $width;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param mixed $length
     */
    public function setLength($length): void
    {
        $this->length = $length;
    }

    public function save()
    {
        $sql = "INSERT INTO product(SKU,name,price) VALUES ('$this->SKU','$this->name','$this->price');
                    INSERT INTO furniture(SKU,height,width,length) VALUES ('$this->SKU','$this->height','$this->width','$this->length')";
        $db = new Database();
        $db->executeStatement($sql);
    }

    public function getProducts()
    {
        $sql = "SELECT product.idproduct,product.SKU,product.name,product.price,furniture.height,furniture.width,furniture.length FROM product 
RIGHT JOIN furniture ON product.SKU = furniture.SKU";
        $db = new Database();
        $stmt = $db->executeStatement($sql);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $res = $stmt->fetchAll();
        $products = array();
        foreach ($res as $product) {
            $prod = new Furniture();
            $prod->setIdProduct($product["idproduct"]);
            $prod->setSKU($product["SKU"]);
            $prod->setName($product["name"]);
            $prod->setPrice($product["price"]);
            $prod->setHeight($product["height"]);
            $prod->setWidth($product["width"]);
            $prod->setLength($product["length"]);
            $products[] = $prod;
        }
        return $products;
    }
}
