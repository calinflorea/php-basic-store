<?php

abstract class Product
{
    protected $idProduct;
    protected $SKU;
    protected $name;
    protected $price;

    /**
     * @return mixed
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * @param mixed $idProduct
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;
    }

    /**
     * @return mixed
     */
    public function getSKU()
    {
        return $this->SKU;
    }

    /**
     * @param mixed $SKU
     */
    public function setSKU($SKU)
    {
        $this->SKU = $SKU;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    public static function deleteProducts($SKUS)
    {
        $deleteSKUS = implode("','", $SKUS);
        $sql = "DELETE FROM `product` WHERE `SKU` IN ('{$deleteSKUS}');";
        $db = new Database();
        $db->executeStatement($sql);
    }

    abstract public function save();

    abstract public function getProducts();
}
