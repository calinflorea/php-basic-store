<?php

class Book extends Product implements JsonSerializable
{
    private $weight;

    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return [
            'SKU' => $this->SKU,
            'name' => $this->name,
            'price' => $this->price . "$",
            'specificAttribute' => "Weight: " . $this->weight . "KG"
        ];
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function save()
    {
        $sql = "INSERT INTO product(SKU,name,price) VALUES ('$this->SKU','$this->name','$this->price');
                    INSERT INTO book(SKU,weight) VALUES ('$this->SKU','$this->weight')";
        $db = new Database();
        $db->executeStatement($sql);
    }

    public function getProducts()
    {
        $sql = "SELECT product.idproduct,product.SKU,product.name,product.price,book.weight FROM product 
RIGHT JOIN book ON product.SKU = book.SKU";
        $db = new Database();
        $stmt = $db->executeStatement($sql);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $res = $stmt->fetchAll();
        $products = array();
        foreach ($res as $product) {
            $prod = new Book();
            $prod->setIdProduct($product["idproduct"]);
            $prod->setSKU($product["SKU"]);
            $prod->setName($product["name"]);
            $prod->setPrice($product["price"]);
            $prod->setWeight($product["weight"]);
            $products[] = $prod;
        }
        return $products;
    }
}
