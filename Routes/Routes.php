<?php

class Routes
{
    public static function urlHandle()
    {
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri = explode('/', $uri);

        if ((isset($uri[0]) && $uri[0] === '') && (isset($uri[1]) && $uri[1] === '') && (count($uri) == 2)) {
            require "Views/ProductList.html";
        } elseif (isset($uri[1]) && $uri[1] === 'add-product') {
            require "Views/AddProduct.html";
        } elseif (isset($uri[1]) && $uri[1] === 'getproducts') {
            $objFeedController = new ProductController();
            $objFeedController->getProducts();
        } elseif (isset($uri[1]) && $uri[1] === 'addproduct') {
            $objFeedController = new ProductController();
            $objFeedController->addProduct();
        } elseif (isset($uri[1]) && $uri[1] === 'deleteproducts') {
            $objFeedController = new ProductController();
            $objFeedController->deleteProducts();
        } else {
            header("HTTP/1.1 404 Not Found");
            exit();
        }
    }
}
