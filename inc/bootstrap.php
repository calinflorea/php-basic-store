<?php

define("PROJECT_ROOT_PATH", __DIR__ . "/../");

// include main configuration file
require_once PROJECT_ROOT_PATH . "/inc/config.php";

// include the controller file
require_once PROJECT_ROOT_PATH . "/Controller/Api/BaseController.php";
require_once PROJECT_ROOT_PATH . "/Controller/Api/ProductController.php";

// include the use model file
require_once PROJECT_ROOT_PATH . "/Model/DB/Database.php";
require_once PROJECT_ROOT_PATH . "/Model/Product.php";
require_once PROJECT_ROOT_PATH . "/Model/DVD.php";
require_once PROJECT_ROOT_PATH . "/Model/Book.php";
require_once PROJECT_ROOT_PATH . "/Model/Furniture.php";

//include the routes file
require_once PROJECT_ROOT_PATH . "/Routes/Routes.php";
